#include <DHT.h>
#define DHTPIN 4     // what digital pin we're connected to
#define DHTTYPE DHT22   // DHT 11
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <ESP_Adafruit_SSD1306.h>
#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

DHT dht(DHTPIN, DHTTYPE);

void setup() {
Serial.begin(115200);
delay(10);
dht.begin();
}

void loop() {

  // Read humidity
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

    Serial.println("");
    Serial.print("Temperature: ");
    Serial.println(t);
    Serial.print("Humidity: ");
    Serial.println(h);

    delay(1000);
    
  }


