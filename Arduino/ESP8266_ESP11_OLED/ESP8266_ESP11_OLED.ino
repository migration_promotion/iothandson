#include <SPI.h>
#include <Wire.h>
#include <DHT.h>
#include <ESP8266WiFi.h>
extern "C" {
  #include "user_interface.h"
}
#include <Adafruit_GFX.h>
#include <ESP_Adafruit_SSD1306.h>
#define DHTPIN 4     // what digital pin we're connected to
#define DHTTYPE DHT22   // DHT 11
#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

const char* ssid     = "openblocks_handson";
const char* password = "iothandson";

const char* host = "maker.ifttt.com";
const char* event = "dht22_google";
const char* secretkey = "oi2GcXYlqaY_hhrPz5utSbx9VqaoR3LTXpbecwEmYtK";
float val1 = 0;
float val2 = 0;
float val3 = 0;

DHT dht(DHTPIN, DHTTYPE);

void setup() {
Wire.begin(14,12);  // (SDA,SCL):ESP8266(IO_0)-OLED(SDA),(IO_1)-OLED(SCL)
display.begin(SSD1306_SWITCHCAPVCC, 0x78>>1); // OLED ADDRESS
display.clearDisplay(); // Clear the buffer.
Serial.begin(115200);
delay(10);
dht.begin();
Serial.println("");
Serial.println("PG start");

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

int value = 0;

void loop() {
// Read humidity
float h = dht.readHumidity();
// Read temperature as Celsius (the default)
float t = dht.readTemperature();
float f = 0.81 * t + 0.01 * h * (0.99 * t - 14.3) + 46.3;
display.clearDisplay();
display.setTextSize(2);
display.setTextColor(WHITE);
display.setCursor(10,5);
display.print(t);
display.println("temp");
display.setCursor(10,25);
display.print(h);
display.println("humi");
display.setCursor(10,45);
display.print(f);
display.println("dis");
display.display();
delay(1000);


float val1 = t;
float val2 = h;
float val3 = f;

  Serial.print("connecting to ");
  Serial.println(host);

  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  const int httpPort = 80;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return;
  }

  // We now create a URI for the request
  String url = "/trigger/";
  url += event;
  url += "/with/key/";
  url += secretkey;
  url += "?value1=";
  url += val1;
  url += "&value2=";
  url += val2;
  url += "&value3=";
  url += val3;

  Serial.print("Requesting URL: ");
  Serial.println(url);

  // This will send the request to the server
  client.print(String("GET ") + url + " HTTP/1.1\r\n" + "Host: " + host + "\r\n" + "Connection: close\r\n\r\n");
  delay(10);

  // Read all the lines of the reply from server and print them to Serial
  while(client.available()){
    String line = client.readStringUntil('\r');
    Serial.print(line);
  }

  Serial.println();
  Serial.println("closing connection");

//DeepSleepモード移行については、IO16 PINとRST PINを繋ぐ必要あり※復旧時に16番の信号によってリセットをかける
Serial.println("DeepSleep start");
// スリープモード移行（復帰タイマー時間の単位は[μ秒]なので、秒を1000,000倍する）
ESP.deepSleep( 300 * 1000 * 1000 );
// スリープモード移行用待機処理

delay( 1000 );
}
