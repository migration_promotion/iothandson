#include <DHT.h>
#define DHTPIN 5     // what digital pin we're connected to
#define DHTTYPE DHT11   // DHT 11

DHT dht(DHTPIN, DHTTYPE);
int smokeA0 = A6;

void setup() {
pinMode(smokeA0, INPUT);
Serial.begin(115200);
delay(10);
dht.begin();
}

void loop() {

  // Read humidity
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  int smokeSensor = analogRead(smokeA0);

//  Serial.print("Pin A0: ");
  Serial.println(smokeSensor);
//  Serial.println("");
//  Serial.print("Temperature: ");
  Serial.println(t);
//  Serial.print("Humidity: ");
  Serial.println(h);
  delay(5000);
    
  }


