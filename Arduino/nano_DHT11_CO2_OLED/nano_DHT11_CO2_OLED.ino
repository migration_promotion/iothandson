#include <SPI.h>
#include <Wire.h>
#include <DHT.h>

#include <Adafruit_GFX.h>
#include <ESP_Adafruit_SSD1306.h>

#include <DHT.h>
#define DHTPIN 5     // what digital pin we're connected to
#define DHTTYPE DHT11   // DHT 11
#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

DHT dht(DHTPIN, DHTTYPE);
int smokeA0 = A6;

void setup() {
Wire.begin(2,3);  // (SDA,SCL):ESP8266(IO_0)-OLED(SDA),(IO_1)-OLED(SCL)
display.begin(SSD1306_SWITCHCAPVCC, 0x78>>1); // OLED ADDRESS
display.clearDisplay(); // Clear the buffer.
pinMode(smokeA0, INPUT);
Serial.begin(115200);
delay(10);
dht.begin();
}

void loop() {

  // Read humidity
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  int smokeSensor = analogRead(smokeA0);

//  Serial.print("Pin A0: ");
  Serial.println(smokeSensor);
//  Serial.println("");
//  Serial.print("Temperature: ");
  Serial.println(t);
//  Serial.print("Humidity: ");
  Serial.println(h);
  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(10,5);
  display.print(smokesensor);
  display.println("GAS");
  display.setCursor(10,25);
  display.print(t);
  display.println("C");
  display.setCursor(10,45);
  display.print(h);
  display.println("%");
  display.display();
  delay(5000);
    
  }


