#include <SPI.h>
#include <Wire.h>
#include <DHT.h>

#include <Adafruit_GFX.h>
#include <ESP_Adafruit_SSD1306.h>
#define DHTPIN 4     // what digital pin we're connected to
#define DHTTYPE DHT22   // DHT 11
#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

DHT dht(DHTPIN, DHTTYPE);
 
void setup() {
Wire.begin(14,12);  // (SDA,SCL):ESP8266(IO_0)-OLED(SDA),(IO_1)-OLED(SCL)
display.begin(SSD1306_SWITCHCAPVCC, 0x78>>1); // OLED ADDRESS
display.clearDisplay(); // Clear the buffer.
Serial.begin(115200);
delay(10);
dht.begin();
}
 
void loop() {
    // Read humidity
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  float f = 0.81 * t + 0.01 * h * (0.99 * t - 14.3) + 46.3;
display.clearDisplay();
display.setTextSize(2);
display.setTextColor(WHITE);
display.setCursor(10,5);
display.print(t);
display.println("temp");
display.setCursor(10,25);
display.print(h);
display.println("humi");
display.setCursor(10,45);
display.print(f);
display.println("dis");
display.display();
delay(5000);
}
