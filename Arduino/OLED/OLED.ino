#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <ESP_Adafruit_SSD1306.h>
#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);
 
void setup() {
Wire.begin(14,12);  // (SDA,SCL):ESP8266(IO_0)-OLED(SDA),(IO_1)-OLED(SCL)
display.begin(SSD1306_SWITCHCAPVCC, 0x78>>1); // OLED ADDRESS
display.clearDisplay(); // Clear the buffer.
}
 
void loop() {
display.setTextSize(2);
display.setTextColor(WHITE);
display.setCursor(30,10);
display.println("Hello");
display.setCursor(30,40);
display.println("World");
display.display();
delay(2000);
}
