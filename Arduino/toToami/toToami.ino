#include <ESP8266WiFi.h>

WiFiClient client;
WiFiClient m2xClient;

const char* ssid     = "machinecom01"; //★接続するWifiのSSID
const char* password = "kindproject01"; //★接続するWifiのパスワード
const char* m2xHost = "https://demo01.to4do.com/Thingworx/Things/Thg_GW_40d3_pqzn/Properties/sdata"; //接続するホストのアドレス
String m2xKey = "ab477103-9e0c-4eae-ba47-b4ebb33da5af";//★M2Xに接続する為のKey

String data = {"sdata:{n01:30}"};



//Toamiにデータを送信する。
void submitToM2X() {

//m2xサーバーに接続する
    if (!m2xClient.connect(m2xHost, 80)) {
      Serial.println("connection failed 1");
      delay(1000);

    }
    //M2xにデータを送信する
    m2xClient.print(String("PUT ") + m2xHost + "json=data\r\n" +
                    "appkey: " + m2xKey + "\r\n" +
                    "Content-Type: application/json;charset=UTF-8\r\n" +
                    "Accept: applocation/json\r\n" +
                    "verify=False\r\n");

    Serial.printf("sent %d to the m2x server\n");
    String response = m2xClient.readString();
    Serial.println(response);
}



void setup(){
  Serial.println("start setup");
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
 
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
 
  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println("end of setup");
}

void loop(){
    submitToM2X();
}
