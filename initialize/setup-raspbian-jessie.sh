#!/bin/bash

# update package and firmware
update_package(){
	# update package
	sudo apt-get update
	# sudo apt-get -y upgrade
	# update firmware
	# sudo rpi-update
}
# install apt-get package
apt_get_install(){
	# setup JAPANESE environment
	sudo apt-get install -y fonts-vlgothic
	sudo apt-get install -y ibus-mozc
	sudo ln –sf /usr/share/zoneinfo/Japan /etc/localtime

	# install editor
	sudo apt-get install -y vim
	
	# install virtual terminal
	sudo apt-get install -y byobu
}

# setup dotfiles
setup_dotfiles(){
	sudo cp ../dotfiles/.bashrc ~/.bashrc
	source ~/.bashrc
	sudo cp -r ../dotfiles/.vim ~/.vim
	sudo cp ../dotfiles/.vimrc ~/.vimrc
	sudo cp ../dotfiles/config.txt /boot/config.txt
	sudo rm  /etc/wvdial.conf
	sudo cp ../dotfiles/dhcpcd.conf /etc/dhcpcd.conf
}

# setup module
setup_module(){
	sudo apt-get install -y cu
	sudo apt-get install -y wvdial
}

# setup Node.js
setup_nodejs(){
	curl -L git.io/nodebrew | perl - setup
	nodebrew install-binary v6.3.0
	nodebrew use v6.3.0
}
setup_TI(){
	sudo apt-get install libcap2-bin
	sudo setcap cap_net_raw+eip $(eval readlink -f `which node`)
}
python_set(){

	sudo apt-get install -y python-dev libbluetooth3-dev
	sudo pip install pybluez
	sudo apt-get install -y libglib2.0 libgtk2.0-dev libboost-python-dev libboost-thread-dev
	sudo pip install gattlib

}
mjpg_set(){

        sudo aptitude install libv4l-dev libjpeg8-dev imagemagick -y
	sudo apt-get install subversion subversion-tools libapache2-svn -y
	svn co https://svn.code.sf.net/p/mjpg-streamer/code/mjpg-streamer /home/pi/iothandson/mjpg-streamer
	cd /home/pi/iothandson/mjpg-streamer
	make
}
fsweb_set(){

	sudo apt install fswebcam -y

}
flame_set(){

	#backup作成日
	DATE=`date +"%Y%m%d"`

	#backup作成
	sudo cp -p /home/pi/.config/lxsession/LXDE-pi/autostart /home/pi/.config/lxsession/LXDE-pi/autostart.$DATE

	#文字を追記
	sed -e "/@point-rpi/a\@xset s 0 0" /home/pi/.config/lxsession/LXDE-pi/autostart.$DATE > /home/pi/.config/lxsession/LXDE-pi/autostart2
	sed -e "/@xset s 0 0/a\@xset s noblank" /home/pi/.config/lxsession/LXDE-pi/autostart2 > /home/pi/.config/lxsession/LXDE-pi/autostart3
	sed -e "/@xset s noblank/a\@xset s noexpose" /home/pi/.config/lxsession/LXDE-pi/autostart3 > /home/pi/.config/lxsession/LXDE-pi/autostart4
	sed -e "/@xset s noexpose/a\@xset dpms 0 0 0" /home/pi/.config/lxsession/LXDE-pi/autostart4 > /home/pi/.config/lxsession/LXDE-pi/autostart
	sudo rm /home/pi/.config/lxsession/LXDE-pi/autostart2
	sudo rm /home/pi/.config/lxsession/LXDE-pi/autostart3
	sudo rm /home/pi/.config/lxsession/LXDE-pi/autostart4
	sudo rm /home/pi/.config/lxsession/LXDE-pi/autostart2

}

START_TIME=`date +%s`

update_package
apt_get_install
setup_dotfiles
setup_module
setup_nodejs
setup_TI
python_set
mjpg_set
fsweb_set
flame_set

END_TIME=`date +%s`

SS=`expr ${END_TIME} - ${START_TIME}`
HH=`expr ${SS} / 3600`
SS=`expr ${SS} % 3600`
MM=`expr ${SS} / 60`
SS=`expr ${SS} % 60`

echo "Total Time: ${HH}:${MM}:${SS} (h:m:s)"
echo "Please reboot"
sudo reboot
