var SPI = require("pi-spi");

var spi = SPI.initialize("/dev/spidev0.0");
var MCP3002 = Buffer([0b01101000,0b00000000]);
console.log("ready for MCP3002");

setInterval(function(){
	spi.transfer(MCP3002, MCP3002.length, function(error,data){
	if(error) console.log(error);
	else{
		var v = ((data[0]<<8)+data[1])& 0b0000001111111111
		console.log("lux value:" +v);
	}
 });
},5000);
