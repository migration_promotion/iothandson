var request = require("request");
var exec = require("child_process").exec
console.log("ready for Polling");

function usboff(){
    exec("sudo ./tool/hub-ctrl.c -P 2 -p 0 ", function(err,stdout,stderr){
		console.log("USB OFF!!");
	});
}

function usbon(){
    exec("sudo ./tool/hub-ctrl.c -P 2 -p 1 ", function(err,stdout,stderr){
		console.log("USB ON!!");
	});
}

function pollingToToami(data){
	var headers = {
		"appkey":"XXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXX",
		"Content-Type":"application/json;charset=UTF-8",
		"Accept":"application/json"	
 	};

 	var options = {
		url: "https://demo01.to4do.com/Thingworx/Things/Thg_GW_XXXX_XXXX/Services/GetGWCommand",
		method: "PUT",
		headers: headers,
		json: data
 	};

 	request(options, function(error,response){
		if(error) console.log(error);
		else {
			console.log("STATUS_CODE: " +response.statusCode);
			console.log("RESULT: "+ response.body.rows[0].result);

	        var result = response.body.rows[0].result
            switch(result){
				case "req_c003,;":
				usboff();
				break;

				case "req_c004,;":
				usbon();
				break;
			}
		}
	 });
}

setInterval(function(){
  var rand = Math.floor(Math.random() * 31) + 20 
  data = {
  	sdata:{
	}
  }

  pollingToToami(data);
  console.log();
 
},5000);

