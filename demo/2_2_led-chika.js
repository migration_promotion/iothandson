var fs = require("fs");

var count = 0

fs.writeFileSync("/sys/class/gpio/export",18);
fs.writeFileSync("/sys/class/gpio/gpio18/direction","out");

function flash(){
	count++;
	fs.writeFileSync("/sys/class/gpio/gpio18/value", count % 2);

	if (count <= 20){
		setTimeout(flash, 1000);
	}else {
		fs.writeFileSync("/sys/class/gpio/gpio18/unexport", 18);
	}
}

flash();
