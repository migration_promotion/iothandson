// 使うライブラリを require する
var Util = require('util');
var Async = require('async');
var SensorTag = require('sensortag');

// set address
var address = "XX:XX:XX:XX:XX:XX"

// SensorTagを見つけた時の処理
function onDiscover(sensorTag) {
  Util.log('Discovered: ' + sensorTag);

  // 切断された時の処理
  sensorTag.on('disconnect', function() {
    Util.log('Disconnected: ' + sensorTag);
  });

  // 接続する
  sensorTag.connectAndSetUp(function(error) {
    if (error) {
      util.log('Connection error:', error);
    } else {
      // 接続できたら準備
      prepareSensor(sensorTag);
    }
  });
};

// SensorTagの準備
function prepareSensor(sensorTag) {
  Async.series([
    function(callback) {
      // 照度センサーの有効化
      Util.log('enableLuxometer');
      sensorTag.enableLuxometer(callback);
    },
    function(callback) {
      // 温度センサーの有効化
      Util.log('enableIrTemperature');
      sensorTag.enableIrTemperature(callback);
    },
    function(callback) {
      // 湿度センサーの有効化
      Util.log('enableHumidity');
      sensorTag.enableHumidity(callback);
    },
    function(callback) {
      // 値が取れるまでちょっと時間が掛かるので待つ
      setTimeout(callback, 2000);
    },
    function(callback) {
      // 5秒毎に readSensorsValue を呼び出すようにする
      setInterval(
        function() { readSensorsValue(sensorTag); },
        5000
      );
      callback();
    }
  ]);
}

// センサーの値を読み取る
function readSensorsValue(sensorTag) {
  Util.log('Reading sensors value: ' + sensorTag)
  sensorData = {};

  Async.series([
    function(callback) {
      // センサーの値を読み取って sensorData に格納
      sensorTag.readLuxometer(function(error, luxometer, humidity) {
        sensorData.luxometer = luxometer;
        callback();
      });
    },
    function(callback) {
      // 温度センサーの値を読み取って sensorData に格納
      sensorTag.readIrTemperature(function(error, objTemp, ambTemp) {
        sensorData.ambTemp = ambTemp;
        callback();
      });
    },
    function(callback) {
      // 湿度センサーの値を読み取って sensorData に格納
      sensorTag.readHumidity(function(error, temperature, humidity) {
        sensorData.humidity = humidity;
        callback();
      });
    },
    function(callback) {
      // 集めたセンサーの値をコンソールに出力
      Util.log(sensorData);
      callback();
    }
  ]);
}

// SensorTagの検索開始
Util.log("discovering... "+ address);
SensorTag.discoverByAddress(address, onDiscover);

