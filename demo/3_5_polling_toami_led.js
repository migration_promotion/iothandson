var request = require("request");
var fs = require("fs");
var exec = require("child_process").exec

var files = [];

files = fs.readdirSync("/sys/class/gpio");

if(/gpio18/.test(files)){

    fs.writeFileSync("/sys/class/gpio/unexport", 18);
}

fs.writeFileSync("/sys/class/gpio/export",18);
fs.writeFileSync("/sys/class/gpio/gpio18/direction","out");

console.log("ready for Polling");

function LEDon(){
	fs.writeFileSync("/sys/class/gpio/gpio18/value", 1);
	console.log("LED on!!");
}

function LEDoff(){
        fs.writeFileSync("/sys/class/gpio/gpio18/value", 0);
        console.log("LED off!!");
}

function pollingToToami(data){
	var headers = {
		"appkey":"XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
		"Content-Type":"application/json;charset=UTF-8",
		"Accept":"application/json"	
 	};

 	var options = {
		url: "https://demo01.to4do.com/Thingworx/Things/Thg_GW_XXXX_XXXX/Services/GetGWCommand",
		method: "PUT",
		headers: headers,
		json: data
 	};

 	request(options, function(error,response){
		if(error) console.log(error);
		else {
			console.log("STATUS_CODE: " +response.statusCode);
			console.log("RESULT: "+ response.body.rows[0].result);

	        var result = response.body.rows[0].result
            switch(result){
				case "req_c003,;":
				LEDon();
				break;

				case "req_c004,;":
				LEDoff();
				break;
			}
		}
	 });
}

setInterval(function(){
  var rand = Math.floor(Math.random() * 31) + 20 
  data = {
  	sdata:{
	}
  }

  pollingToToami(data);
  console.log();
 
},1000);

