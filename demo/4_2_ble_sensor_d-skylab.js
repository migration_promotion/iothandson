// 使うライブラリを require する
var Util = require('util');
var Async = require('async');
var SensorTag = require('sensortag');
var address = "XX:XX:XX:XX:XX:XX"
// ↓ の行を追加
var Http = require('http');

function postData(json_text) {
  var http_options = {
    /* ここは環境に合わせて書き換える↓ */
    hostname: 'xxx.xxx.xxx.xxx',
    port: 9200,
    path: '/sensordata/sensortag/',
    method: 'POST',
    headers: { 'Content-Length': Buffer.byteLength(json_text) }
  };

  var post_req = Http.request(http_options, function(res) {
    res.setEncoding('utf8');
    res.on('data', function(chunk) { Util.log('Response: ' + chunk); });
  });

  Util.log('POST: ' + json_text);
  post_req.write(json_text);
  post_req.end();
}

// SensorTagを見つけた時の処理
function onDiscover(sensorTag) {
  Util.log('Discovered: ' + sensorTag);

  // 切断された時の処理
  sensorTag.on('disconnect', function() {
    Util.log('Disconnected: ' + sensorTag);
  });

  // 接続する
  sensorTag.connectAndSetUp(function(error) {
    if (error) {
      util.log('Connection error:', error);
    } else {
      // 接続できたら準備
      prepareSensor(sensorTag);
    }
  });
};

// SensorTagの準備
function prepareSensor(sensorTag) {
  Async.series([
    function(callback) {
      // 照度センサーの有効化
      Util.log('enableLuxometer');
      sensorTag.enableLuxometer(callback);
    },
    function(callback) {
      // 温度センサーの有効化
      Util.log('enableIrTemperature');
      sensorTag.enableIrTemperature(callback);
    },
    function(callback) {
      // 湿度センサーの有効化
      Util.log('enableHumidity');
      sensorTag.enableHumidity(callback);
    },
    function(callback) {
      // 値が取れるまでちょっと時間が掛かるので待つ
      setTimeout(callback, 2000);
    },
    function(callback) {
      // 5秒毎に readSensorsValue を呼び出すようにする
      setInterval(
        function() { readSensorsValue(sensorTag); },
        5000
      );
      callback();
    }
  ]);
}

// センサーの値を読み取る
function readSensorsValue(sensorTag) {
  Util.log('Reading sensors value: ' + sensorTag)
  sensorData = {};
  // これ↓ を追加
  sensorData.timestamp = new Date().toISOString();

  Async.series([
  function(callback){
      // 照度センサーの値を読み取って sensorData に格納
      sensorTag.readLuxometer(function(error, luxometer, humidity) {
        sensorData.luxometer01 = luxometer;
        callback();
      });
    },
    function(callback) {
      // 温度センサーの値を読み取って sensorData に格納
      sensorTag.readIrTemperature(function(error, objTemp, ambTemp) {
        sensorData.ambTemp01 = ambTemp;
        callback();
      });
    },
    function(callback) {
      // 湿度センサーの値を読み取って sensorData に格納
      sensorTag.readHumidity(function(error, temperature, humidity) {
        sensorData.humidity01 = humidity;
        callback();
      });
    },
    function(callback) {
      // 集めたセンサーの値をコンソールに出力
      Util.log(sensorData);
      /* これ↓を追加 */
      postData(JSON.stringify(sensorData));
      callback();
    }
  ]);
}

// SensorTagの検索開始
Util.log("discovering... "+ address);
SensorTag.discoverByAddress(address, onDiscover);
