// 使うライブラリを require する
var Util = require('util');
var Async = require('async');
var SensorTag = require('sensortag');
var Https = require('https');
var address = "24:71:89:E8:40:03"
// ↓の行を追加 
var request = require("request");

function postData(json_text) {
  var https_options = {
    // ここは環境に合わせて書き換える↓ 
    hostname: 'xxxxxxxxxxxxxxx,
    port: 443,
    path: '/geo_test/geo_type/',
    method: 'POST',
    headers: { 'Content-Length': Buffer.byteLength(json_text) }
  };

  var post_req = Https.request(https_options, function(res) {
    res.setEncoding('utf8');
   res.on('data', function(chunk) { Util.log('Response: ' + chunk); });
  });

  Util.log('POST: ' + json_text);
  post_req.write(json_text);
  post_req.end();
}

// send to toami
function SendDataToToami(data){
  var headers = {
    "appkey":"XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXX",
    "Content-Type":"application/json;charset=UTF-8",
    "Accept":"application/json"     
  };

  var options = {
	url: "https://demo01.to4do.com/Thingworx/Things/Thg_GW_XXXX_XXXX/Properties/sdata",
	method: "PUT",
	headers: headers,
	json: data
  };

  request(options, function(error,response){
	if(error) util.log(error);						
	else {
 		Util.log("STATUS_CODE:" +response.statusCode);
	    Util.log(data);
		console.log();
	}
  });
}

// SensorTagを見つけた時の処理
function onDiscover(sensorTag) {
  Util.log('Discovered: ' + sensorTag);

  // 切断された時の処理
  sensorTag.on('disconnect', function() {
    Util.log('Disconnected: ' + sensorTag);
  });

  // 接続する
  sensorTag.connectAndSetUp(function(error) {
    if (error) {
      util.log('Connection error:', error);
    } else {
      // 接続できたら準備
      prepareSensor(sensorTag);
    }
  });
};

// SensorTagの準備
function prepareSensor(sensorTag) {
  Async.series([
    function(callback) {
      // 温度センサーの有効化
      Util.log('enableIrTemperature');
      sensorTag.enableIrTemperature(callback);
    },
    function(callback) {
      // 湿度センサーの有効化
      Util.log('enableHumidity');
      sensorTag.enableHumidity(callback);
    },
    function(callback) {
      // 照度センサーの有効化
      Util.log('enableLuxometer');
      sensorTag.enableLuxometer(callback);
	},
	function(callback) {
      // 値が取れるまでちょっと時間が掛かるので待つ
      setTimeout(callback, 2000);
    },
    function(callback) {
      // 5秒毎に readSensorsValue を呼び出すようにする
      setInterval(
        function() { readSensorsValue(sensorTag); },
        5000
      );
      callback();
    }
  ]);
}

// センサーの値を読み取る
function readSensorsValue(sensorTag) {
  Util.log('Reading sensors value: ' + sensorTag)
  sensorData = {};
  sensorData.timestamp = new Date().toISOString();

  Async.series([
    function(callback) {
      // 温度センサーの値を読み取って sensorData に格納
      sensorTag.readIrTemperature(function(error, objTemp, ambTemp) {
        sensorData.ambTemp = ambTemp;
        callback();
      });
    },
    function(callback) {
      // 湿度センサーの値を読み取って sensorData に格納
      sensorTag.readHumidity(function(error, temperature, humidity) {
        sensorData.humidity = humidity;
        callback();
      });
    },
    function(callback) {
      // 照度センサーの値を読み取って sensorData に格納
      sensorTag.readLuxometer(function(error, luxometer, humidity) {
        sensorData.luxometer = luxometer;
        callback();
      });
    },
    function(callback) {
      // batteryの値を読み取って sensorData に格納
      sensorTag.readBatteryLevel(function(error, battery) {
        sensorData.battery = battery;
        callback();
      });
    },
	function(callback) {
      // 集めたセンサーの値をコンソールに出力
      Util.log(sensorData);
      postData(JSON.stringify(sensorData));

      // toami data     
      data = {
	  	sdata:{
			n01: sensorData.ambTemp01,
			n02: sensorData.humidity01,
			n03: sensorData.luxometer01
		}
	  
	  }
     // SendDataToToami(data)
      callback();
    }
  ]);
}

// SensorTagの検索開始
Util.log("discovering..."+ address);
SensorTag.discoverByAddress(address, onDiscover);
