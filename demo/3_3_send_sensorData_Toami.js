var request = require("request");
var SPI = require("pi-spi");

var spi = SPI.initialize("/dev/spidev0.0");
var MCP3002 = Buffer([0b01101000,0b00000000]);
console.log("ready for MCP3002");

function SendDataToToami(data){
 var headers = {
	"appkey":"XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXX",
	"Content-Type":"application/json;charset=UTF-8",
	"Accept":"application/json"	
 };


 var options = {
	url: "https://demo01.to4do.com/Thingworx/Things/Thg_GW_XXXX_XXXX/Properties/sdata",
	method: "PUT",
	headers: headers,
	json: data
 };

 request(options, function(error,response){
	if(error) console.log(error);
	else {
		console.log("STATUS_CODE:" +response.statusCode);
		console.log(data);
	//	console.log(response);
	}
 });
}


setInterval(function(){
  spi.transfer(MCP3002, MCP3002.length, function(error,data){
  if(error) console.log(error);
  else{
  var v = ((data[0]<<8)+data[1])& 0b0000001111111111
  
  data = {
  	sdata:{
		n01:v
	}
  }

  SendDataToToami(data);
  console.log();
  }
 });
},5000);
