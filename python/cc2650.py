#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-

from __future__ import print_function

import sys
import ctypes
import time
import struct
import requests
from gattlib import GATTRequester, DiscoveryService

class SensorTag(object):
    def __init__(self, address):
        self.requester = GATTRequester(address, False)
        self.humidity = float(0.0)
        self.temperature = float(0.0)
        self.object_temperature = float(0.0)
        self.barometer = float(0.0)
        self.lux = float(0.0)

    def connect(self):
        time.sleep(1)
	self.requester.connect(True)

    def check_status(self):
        status = "connected" if self.requester.is_connected() else "not connected"
        print("Checking current status: {}".format(status))

    def disconnect(self):
        print(str("Disconnecting..."))
        self.requester.disconnect()
        print("Succeed.")

    def enable_humidity(self, enable):
        status = '\x01' if enable else '\x00'
        self.requester.write_by_handle(0x2f, status)

    def check_humidity(self):
        time.sleep(1)
        raw_data = self.requester.read_by_handle(0x2c)[0]
        raw_temp = (ord(raw_data[1]) << 8) + ord(raw_data[0])
        raw_humi = (ord(raw_data[3]) << 8) + ord(raw_data[2])
        self.temperature = round((float(raw_temp) / float(65536)) * 165 - 40, 1)
        self.humidity = round((float(raw_humi) / float(65536)) * 100, 1)

    def enable_IRtemperature(self, enable):
        status = '\x01' if enable else '\x00'
        self.requester.write_by_handle(0x27, status)

    def check_IRtemperature(self):
        time.sleep(1)
        raw_data = self.requester.read_by_handle(0x24)[0]
        raw_obj = (ord(raw_data[1]) << 8) + ord(raw_data[0])
        raw_amb = (ord(raw_data[3]) << 8) + ord(raw_data[2])
        self.object_temperature = round((float(raw_obj) / 4.0 ) * 0.03125, 1)
        self.temperature = round((float(raw_amb) / 4.0 ) * 0.03125, 1)

    def enable_Barometer(self, enable):
        status = '\x01' if enable else '\x00'
        self.requester.write_by_handle(0x37, status)

    def check_Barometer(self):
        time.sleep(1)
        raw_data = self.requester.read_by_handle(0x34)[0]
        raw_temp = (ord(raw_data[2]) << 16) + (ord(raw_data[1]) << 8) + ord(raw_data[0])
        raw_baro = (ord(raw_data[5]) << 16) + (ord(raw_data[4]) << 8) + ord(raw_data[3])
        self.temperature = round(float(raw_temp) / 100.0, 1)
        self.barometer = round(float(raw_baro) / 100.0, 1)

    def enable_Optical(self, enable):
        status = '\x01' if enable else '\x00'
        self.requester.write_by_handle(0x47, status)

    def check_Optical(self):
        time.sleep(1)
        raw_data = self.requester.read_by_handle(0x44)[0]
        raw_lux = (ord(raw_data[1]) << 8) + ord(raw_data[0])
        raw_lux_m = raw_lux & 0b0000111111111111
        raw_lux_e = (raw_lux & 0b1111000000000000) >> 12
        self.lux = raw_lux_m * (0.01 * pow(2.0,raw_lux_e))


class DiscoverDevice():
    def GetDeviceList(self):
        service = DiscoveryService("hci0")
        device = service.discover(2)
        return device

#if __name__ == '__main__':

    print("started! CC2650")

while 1:

    tag = SensorTag("24:71:89:E8:5F:84")
    tag.connect()

    tag.enable_humidity(True)
    tag.check_humidity()
    tag.enable_IRtemperature(True)
    tag.check_IRtemperature()
    tag.enable_Barometer(True)
    tag.check_Barometer()
    tag.enable_Optical(True)
    tag.check_Optical()
    print("温度：",tag.temperature,"℃ ")
    print("湿度：",tag.humidity,"% ")
    print("照度：",tag.lux,"lx")

