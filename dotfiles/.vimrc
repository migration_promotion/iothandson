syntax on
colorscheme molokai
set t_Co=256
set nobackup
set encoding=utf-8
set fileencoding=utf-8
set autoindent
set number 
set incsearch
set wrapscan
set ignorecase
set showmatch
set showmode
set title
set ruler
set tabstop=4

set clipboard+=unnamed
set clipboard=unnamed

