'use strict';

const noble = require('noble');
const serviceuuid = `82a49d446d0411e8adc0fa7ae01bbebc`;
const charauuid = `82a49d446d0411e8adc0fa7ae01bbebc`;

//キャラクタリスティックにアクセスしてデータやりとり
const accessChara = (chara) => {
    console.log('-----Start GATT Access-----')
    chara.notify(true, (err) => {
        if (err) {
          console.log('listen notif error', err)
        } else {
          console.log('listen notif')
        }
    });
    chara.on('data', (data, isNotif) => {
	var data = result.replace(",","");
        console.log(data);
    });
}


//discovered BLE device
const discovered = (peripheral) => {
    console.log(`BLE Device Found: ${peripheral.advertisement.localName}(${peripheral.uuid}) RSSI${peripheral.rssi}`);

    if(peripheral.advertisement.localName === 'PERCUL01'){
        noble.stopScanning();
        console.log('device found');
        console.log(`service discover...`);

        peripheral.connect(error => {
            if (error) {
                console.log("connection error:", error)
            } else {
                console.log("device connected");
            }

            peripheral.discoverServices([],(err, services) => {
                if (error) {
                    console.log("discover service error", error)
                }
                console.log('discover service');               
                services.forEach(service => {
                    if(service.uuid === serviceuuid){
                        service.discoverCharacteristics([], (error, charas) => {
                            console.log('discover chara');
                            charas.forEach(chara => {
                                if(chara.uuid === charauuid){
                                    console.log("found chara: ", chara.uuid)
                                    accessChara(chara);
                                }
                            });
                        });
                    }
                });
            });
        });
    }
}

//BLE scan start
const scanStart = () => {
    noble.startScanning();
    noble.on('discover', discovered);
}

if(noble.state === 'poweredOn'){
    scanStart();
}else{
    noble.on('stateChange', scanStart);
}
