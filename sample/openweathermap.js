var http = require('http');
var fs = require('fs');

var location = "Tokyo,jp";
var units = 'metric';
var APIKEY = "cf15fcf30c994b4eaafdbadf5c559c6d";

var URL = 'http://api.openweathermap.org/data/2.5/weather?q='+ location +'&units='+ units +'&appid='+ APIKEY;

http.get(URL, function(res) {
  var body = '';
  res.setEncoding('utf8');

  res.on('data', function(chunk) {
    body += chunk;
  });

  res.on('data', function(chunk) {
    res = JSON.parse(body);
    console.log(res);
    fs.writeFile('test.json', JSON.stringify(res.main, null, '    '));
  });
}).on('error', function(e) {
  console.log(e.message);
});
