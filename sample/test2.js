'use strict';

var Util = require('util');
var Http = require('http');
var request = require("request");
const noble = require('noble');
const serviceuuid = `82a49d446d0411e8adc0fa7ae01bbebc`;
const charauuid = `82a4a0006d0411e8adc0fa7ae01bbebc`;


// send to toami
function SendDataToToami(data){
  var headers = {
    "appkey":"d135f4b5-0561-43cd-b2cd-83766cb3b812",
    "Content-Type":"application/json;charset=UTF-8",
    "Accept":"application/json"     
  };

  var options = {
        url: "https://demo01.to4do.com/Thingworx/Things/Thg_GW_40d3_kr92/Properties/sdata",
        method: "PUT",
        headers: headers,
        json: data
  };

  request(options, function(error,response){
        if(error) util.log(error);
        else {
                Util.log("STATUS_CODE:" +response.statusCode);
            Util.log(data);
                console.log();
        }
  });
}


//discovered BLE device
const discovered = (peripheral) => {
    console.log(`BLE Device Found: ${peripheral.advertisement.localName}(${peripheral.uuid}) RSSI${peripheral.rssi}`);

    if(peripheral.advertisement.localName === 'PERCUL01'){
        noble.stopScanning();
        console.log('device found');
        console.log(`service discover...`);

        peripheral.connect(error => {
            if (error) {
                console.log("connection error:", error)
            } else {
                console.log("device connected");
            }

            peripheral.discoverServices([],(err, services) => {
                if (error) {
                    console.log("discover service error", error)
                }
                console.log('discover service');
                services.forEach(service => {
                    if(service.uuid === serviceuuid){
                        service.discoverCharacteristics([], function(error, characteristics) {

	              		//get notify data
        	      		characteristics[0].on('data', function(data, isNotification) {
               				var result = "";
               				for(var i=0; i<data.length; i++){
                  				result += data[i] + ',';
                			}
                			testFunction(result);
              			});
              			characteristics[0].subscribe(function(error) {
                			console.log('notify');
              			});
                        	console.log('discover chara');
                        });
                    }
                });
            });
        });
    }
}

//BLE scan start
const scanStart = () => {
    noble.startScanning();
    noble.on('discover', discovered);
}

if(noble.state === 'poweredOn'){
    scanStart();
}else{
    noble.on('stateChange', scanStart);
}


// send Data
function testFunction (result) {
    var data = result.replace(",","");
//　　var V = data * 5 / 256;
//    var length = 0;
//
//
//    if (V >= 0.4 && V < 2.0){
//    length = 1 / ((V - 0.1)/56.6);
//
//    }else if (V >= 2.0 && V <= 2.55){
//    length = 1 / ((V-0.9)/33);
//
//    }
    if (data == 255 && data == 0){

    console.log(data);
    console.log("unsend!"); 

    }else{

    console.log(data);
//    console.log(length);

    // toami data     
    	data = {
        	      sdata:{
                	      n25: data
              	}

    	}
    SendDataToToami(data)

    }

    return;
}
