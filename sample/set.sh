#!/bin/bash

while :
do
cd /home/pi/iothandson/mjpg-streamer/

echo ""
echo "------------------------------------------------------"
echo "30秒間動画を表示します"
echo "------------------------------------------------------"
echo ""

sleep 4;sudo sh /home/pi/iothandson/mjpg-streamer/start.sh &
sleep 1;chromium-browser --start-maximized &

sleep 30;sudo pkill mjpg_streamer
sudo pkill -f chromium-browser

sleep 2

echo ""
echo "------------------------------------------------------"
echo "10秒間停止します。クラウドに静止画を投げるならこの間に"
echo "------------------------------------------------------"
echo ""
echo ""
echo "------------------------------------------------------"
echo "一旦、/home/pi/image.img に静止画を撮影し保存します。"
echo "------------------------------------------------------"
echo ""

sleep 10;fswebcam image.jpg

echo ""
echo "------------------------------------------------------"
echo "/home/pi/ 配下に「image.img」を保存しました。"
echo "------------------------------------------------------"
echo ""

sleep 5

done
