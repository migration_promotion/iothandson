var noble = require('noble');
var DEVICE_NAME = "PERCUL01";
var SERVICE_UUID = "82a49d446d0411e8adc0fa7ae01bbebc";
//超音波
var SERVICE_CHARACTERISTIC_UUID = "82a4a0006d0411e8adc0fa7ae01bbebc";
//赤外線
var SERVICE_CHARACTERISTIC_UUID2 = "82a4a4f66d0411e8adc0fa7ae01bbebc";

//start ble
noble.on('stateChange', function(state) {
  if (state === 'poweredOn') {
    noble.startScanning();
  } else {
    noble.stopScanning();
  }
});

//search ble
noble.on('discover', function(peripheral) {

    //equals devicename
    if(peripheral.advertisement.localName == DEVICE_NAME){
        noble.stopScanning();

        //connect
        peripheral.connect(function(error){
          console.log("connect");

          //find service
          peripheral.discoverServices(SERVICE_UUID, function(error, services) {
            var deviceInformationService = services[0];

            //find CharacteristicService
            deviceInformationService.discoverCharacteristics([], function(error, characteristics) {

              //get notify data
              characteristics[0].on('data', function(data, isNotification) {
                var result = "";
                for(var i=0; i<data.length; i++){
                  result += data[i] + ',';
                }
                testFunction(result);
              });
              characteristics[0].subscribe(function(error) {
                console.log('notify');
              });
              
              //get notify data2
              characteristics[1].on('data', function(data, isNotification) {
                var result = "";
                for(var i=0; i<data.length; i++){
                  result += data[i] + ',';
                }
                testFunction2(result);
              });
              characteristics[1].subscribe(function(error) {
                console.log('notify');
              });
            });
          });
        });
        peripheral.disconnect(function(error){
          console.log("disconnected from peripheral");
          console.log("Start Scaning");
          noble.startScanning();
        });
    }
});

// send Data
function testFunction (result) {
    var data = result.replace(",","");
//    node.send({payload:data});
    console.log(data);
    return;
}

// send Data
function testFunction2 (result) {
    var data = result.replace(",","");
    data = data + ":test2"
//    node.send({payload:data});
    console.log(data);
    return;
}

