// 使うライブラリを require する
var Util = require('util');
var Async = require('async');
var Http = require('http');
var request = require("request");
var serialport = require('serialport');
var portName = '/dev/ttyUSB0';
var sp = new serialport.SerialPort(portName, {
    baudRate: 57600,
    dataBits: 8,
    parity: 'none',
    stopBits: 1,
    flowControl: false,
    parser: serialport.parsers.readline("\n")
});


//function postData(json_text) {

// send to toami
function SendDataToToami(data){
  var headers = {
    "appkey":"e3eae269-0379-4b51-b1b9-cf96c339ad3b",
    "Content-Type":"application/json;charset=UTF-8",
    "Accept":"application/json"     
  };

  var options = {
	url: "https://demo01.to4do.com/Thingworx/Things/Thg_GW_tduy_h6yx/Properties/sdata",
	method: "PUT",
	headers: headers,
	json: data
  };

  request(options, function(error,response){
	if(error) util.log(error);						
	else {
 		Util.log("STATUS_CODE:" +response.statusCode);
	    Util.log(data);
		console.log();
	}
  });
}

sp.on('data', function(input) {

    var buffer = new Buffer(input, 'utf8');
    var enn = buffer.toString('hex', 7,8);
    switch (enn){
      case '08':
      data = {
                sdata:{
                        n04: enn
                }
          }
      SendDataToToami(data)

      case '09':
      data = {
                sdata:{
                        n04: enn
                }
          }
      SendDataToToami(data)

      case '':
    }

    }
  );
